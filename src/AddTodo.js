import React, {useState} from 'react';
import styled from 'styled-components';

//Styled Components
const FilterSpan = styled.span `
  color: #422057FF;
  margin: 10px;
    `;

const FormHolder = styled.form `
  margin-top: 15px;
    text-align: center;
    margin-left: auto;
    margin-right: auto;`;

//Functional Component
const AddTodo = ({addTodo}) => {
  const [state, setState] = useState('');
//Logic of adding a todo to a state, and then it clears the field where we put the text
  const handleSubmit = (e) => {
    e.preventDefault();
    addTodo(state);
    setState('');
  }
    return (
      // On every input field change it sets the text which user has passed in into a new state
      <div>
          <FormHolder onSubmit={handleSubmit}>
            <FilterSpan className="filter-content">Add new Todo :</FilterSpan><br/>
            <input type="text" aria-label="inputField" maxlength="25" value={state} onChange={(e) => {setState(e.target.value)}}/>
          </FormHolder>
      </div>
    )
}
export default AddTodo
