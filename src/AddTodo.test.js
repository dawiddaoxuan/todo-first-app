import React from 'react';
import {render, fireEvent} from '@testing-library/react';
import "@testing-library/jest-dom";
import AddTodo from './AddTodo';

const rendering = () => {
    const create = render(<AddTodo />)
    const input = create.getByLabelText('inputField')
    return {
      input,
      ...create,
    }

  }
  it('simulates adding a todo', () => {
    const { input } = rendering()
    fireEvent.change(input, { target: { value: 'Buy some milk' } })
    expect(input.value).toBe('Buy some milk')
  })

  it('simulates another todo', () => {
    const { input } = rendering()
    fireEvent.change(input, { target: { value: 'Another test' } })
    expect(input.value).toBe('Another test')
  })
