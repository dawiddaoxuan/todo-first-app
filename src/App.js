import './App.css';
import React, {useState} from 'react';
import Todos from './Todos';
import AddTodo from './AddTodo';
import Filter from './Filter';
import styled from 'styled-components';

const FILTER_SHOW_ALL = "Show all ";
const FILTER_SHOW_DONE = "Show done";
const FILTER_SHOW_UNDONE = "Show what's not done yet";

// Styled Components

const H1 = styled.h1 `
  text-align: center;
    font-size: 36px;
    color: #422057FF;
`;
const AppHolder = styled.div `
    background-color: #FCF951FF;
    width: fit-content;
    max-width: 500px;
    border: 2px solid #422057FF;
    border-radius: 10%;
    margin-left: auto;
    margin-right: auto;
    margin-top: 10px;
`;

// Functional Component Starts

const App = () => {
  // Setting two elements of state


  function useLocalStorage (localData) {
    const getLocalStorage = localStorage.getItem(localData);

    const [loc, setState] = useState(getLocalStorage ? JSON.parse(getLocalStorage) : {todos: []})

    function setLocal(newItem) {
      localStorage.setItem(localData , JSON.stringify(newItem));
      setState(newItem)
    }
    return [loc, setLocal];
  }
  const [state, setState] = useLocalStorage('todos');
  const [stateFilter, setFilter] = useState({
    filter: FILTER_SHOW_ALL
  });

  // Main logic of application, means functions

   const deleteTodo = (id) => {
    const todos = state.todos.filter(todo => {
      return todo.id !== id
    });
    setState({
      ...state, todos
    })
  };
   const addTodo = (content) => {
    let todos = [...state.todos, {content, id: Math.random(), isDone: false}];
    setState({
      ...state, todos
    })
  };
   const toggleArchiveTodo = (todo) => {
    todo.isDone = !todo.isDone
    setState({
      // ..state, todo because hooks is not merging state automatically so we have to do it manually
      ...state, todo
    });
  };
  // This function should filter through the array of todos to show only those which we chosed.
  const filterTodo = (filter) =>{
    setFilter({
      filter
    });
};
  const showFilteredTodo = () => {
    const todos = state.todos;
    switch(stateFilter.filter) {
      case FILTER_SHOW_ALL:
        default:
        return todos;
      case FILTER_SHOW_DONE:
        return todos.filter((todo) => todo.isDone === true);
      case FILTER_SHOW_UNDONE:
        return todos.filter((todo) => todo.isDone === false);
    }
  };

    return (
      // Calling Components to be shown on site, and passing the functions as a props to the other files, what gives us a possibility to fire those functions in other Components whenever we want
      <AppHolder>
        <H1>Todos</H1>
        <Todos
        todos={showFilteredTodo(state.todos)}
        deleteTodo={deleteTodo}
        toggleArchiveTodo={toggleArchiveTodo}
        filter={stateFilter}/>
        <AddTodo
        addTodo={addTodo} />
        <Filter
        FILTER_SHOW_ALL={FILTER_SHOW_ALL}
        FILTER_SHOW_DONE={FILTER_SHOW_DONE}
        FILTER_SHOW_UNDONE={FILTER_SHOW_UNDONE}
        filterTodo={filterTodo}
        />
      </AppHolder>
    );
}

export default App;
