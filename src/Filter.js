import React from 'react';
import styled from 'styled-components';

//Styled Components
const FilterContainer = styled.div `
    margin-left: auto;
    margin-right: auto;
    width: fit-content;
    margin-bottom: 10px;
    text-align: center;
    `;

const FilterSpan = styled.span `
  color: #422057FF;
  margin: 10px;
    `;

// Start of function component, on click it changes the filter in app.js
 const Filter = ({filterTodo, FILTER_SHOW_ALL, FILTER_SHOW_DONE, FILTER_SHOW_UNDONE}) => {
  return(
    <FilterContainer>
     <FilterSpan>Filter: </FilterSpan><br/>
     <FilterSpan onClick={() => {filterTodo(FILTER_SHOW_ALL)}}>{FILTER_SHOW_ALL} </FilterSpan><br/>
     <FilterSpan onClick={() => {filterTodo(FILTER_SHOW_DONE)}}>{FILTER_SHOW_DONE} </FilterSpan><br/>
     <FilterSpan onClick={() => {filterTodo(FILTER_SHOW_UNDONE)}}>{FILTER_SHOW_UNDONE} </FilterSpan>
    </FilterContainer>
    );
}
export default Filter