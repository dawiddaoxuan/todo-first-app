import React from 'react';
import styled from 'styled-components';

//Styled Components
const Button = styled.button`
    margin-left: 5px;
    background-color: #422057FF;
    border-radius: 40px;
    border: 1px solid #422057FF;
    font-size: 12px;
    color: #FCF951FF;
    &:hover {
      background-color: black;
    }
`;
const Span = styled.span`
    margin: 10px;
    text-align: center;
    word-wrap: break-word;
`;
const CheckboxHolder = styled.div `
  float: right;
`;
const ContentHolder = styled.div `
    margin: 10px;
    min-height: 30px;
    height: auto;
    color: #422057FF;
    border: 1px solid #422057FF;
    border-radius: 30px;

`;
const Paragraph = styled.p`
    color: #422057FF;
    text-align: center;
`;
// Start of the function Component
const Todos = ({todos, deleteTodo, toggleArchiveTodo}) => {
  //Checks if our array have an data inside
  const todoList = todos.length ? (
    // maps our todos array into new array where every single object is an "todo"
    todos.map(todo => {
        return (
          //Content of our single todo object
        <ContentHolder key={todo.id}>
        <Button onClick={() => {deleteTodo(todo.id)}}>x</Button>
          <Span>{todo.content}</Span>
          <CheckboxHolder>
          <input type="checkbox" value= {todo.isArchived} onChange={() => {toggleArchiveTodo(todo)}}/><Span>Done</Span>
          </CheckboxHolder>
        </ContentHolder>
      )
    })
  ) : (
    <Paragraph>You have no todos left</Paragraph>
  )
  return (
    // renders the whole list with every single todo inside
    <div className="todos collection">
      {todoList}
    </div>
  )
}
export default Todos
