import React from 'react';
import {render} from '@testing-library/react';
import "@testing-library/jest-dom";
import Todos from './Todos';

it ('renders a paragraph when list is empty', () => {
  render(<Todos todos/>);
  const list = document.querySelector('.todos');
  expect(list.textContent).toBe('You have no todos left')
});